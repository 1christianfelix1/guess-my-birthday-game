# Process:
# Greet User and ask for name
# Guess user's birthday only 4 times
# Output message if guess is right
# Output message if guess is wrong

# Use random function to generate values between 1-12 (months)
# Use random function to generate values between 1924-2004 (valid years)
# Compare month that the computer generates to user's birthday

from random import randint

# Get user's name
user_name = input("Hi! What is your name? ")

# Creator's birthdate
month = 12
year = 1997

# Guess counter
guess_count = 1

# Non-functional requirements
# You can only use the following Python features. This may seem ridiculous, but we want you to think of a program in the simplest constructs you can.

# Variables: You can create as many variables as you want and put data into and get data out of them.
# Strings: You can use strings. A program without strings is like a dinner without food.
# Integers: The computer is trying to guess the year you were born in, which is likely an integer, so it's good we can use those.
# if: You can use and of the if-elif-else features of the Python language.
# print: So you can print things.
# input: So you can respond to the computer's prompts.
# exit: So the game can stop immediately
# int: So you can turn strings into numbers.
# randint: A function to generate random numbers.


for guess_count in range(5):
    # Generators
    guess_month = randint(1, 12)
    guess_year = randint(1924, 2004)

    print("Guess ", guess_count+1, " : ", user_name,
          " were you born in ", guess_month, " / ", guess_year, " ?")
    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    if guess_count != 4:
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")


# Loop for guessing
# while guess_count <= 5:
#     # Generators
#     guess_month = randint(1, 12)
#     guess_year = randint(1924, 2004)
#     print("Guess ", guess_count, " : ", user_name,
#           " were you born in ", guess_month, " / ", guess_year, " ?")
#     response = input("yes or no? ")
#     if response == "yes":
#         print("I knew it!")
#         exit()
#     else:
#         if guess_count < 5:
#             print("Drat! Lemme try again!")
#         guess_count += 1
# print("I have other things to do. Good bye.")
